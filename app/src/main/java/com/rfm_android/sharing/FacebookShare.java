/*
 * Copyright 2004 - Present Facebook, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.rfm.android.sharing;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.Facebook.DialogListener;
import com.facebook.android.FacebookError;
import com.facebook.android.Util;

public class FacebookShare {

	/*
	 * Your Facebook Application ID must be set before running this example See
	 * http://www.facebook.com/developers/createapp.php
	 */
	public static final String APP_ID = "146361572235432"; 

	ProgressDialog dialog;
	final int AUTHORIZE_ACTIVITY_RESULT_CODE = 27;

	String caption = "caption";
	String description = "description";
	String picture = Utility.HACK_ICON_URL;
	String name = "name";

	String[] main_items = { "Update Status" };
	String[] permissions = { "offline_access", "publish_stream", "read_stream" };

	Activity initActivity;

	  public FacebookShare (Activity initActivity, String caption, String description, String picture, String name)
	    {
	    	this.initActivity= initActivity;
	    	this.caption = caption;
	    	this.description = description;
	    	this.picture = picture;
	    	this.name = name;
	    }
	public void init() {
		if (APP_ID == null) {
			Util.showAlert(
					initActivity,
					"Warning",
					"Facebook Applicaton ID must be "
							+ "specified before running this example: see FbAPIs.java");
			return;
		}

		// Create the Facebook Object using the app id.
		Utility.mFacebook = new Facebook(APP_ID);

		// Instantiate the asynrunner object for asynchronous api calls.
		Utility.mAsyncRunner = new AsyncFacebookRunner(Utility.mFacebook);

		// restore session if one exists
		SessionStore.restore(Utility.mFacebook,
				initActivity.getApplicationContext());

		if (Utility.mFacebook.isSessionValid()) {
			Log.d("session", "valide");
			sendStatus(caption, description, picture, name);
		} else {
			Utility.mFacebook.authorize(initActivity, permissions,
					Facebook.FORCE_DIALOG_AUTH, new LoginDialogListener());
		}
	}

	private void sendStatus(String caption, String description, String picture,
			String name) {
		Bundle params = new Bundle();
		params.putString("caption", caption);
		params.putString("description", description);
		params.putString("picture", picture);
		params.putString("name", name);

		Utility.mFacebook.dialog(initActivity, "feed", params,
				new UpdateStatusListener());
		String access_token = Utility.mFacebook.getAccessToken();
		Log.d("access_token", access_token);
	}

	/*
	 * callback for the feed dialog which updates the profile status
	 */
	public class UpdateStatusListener extends BaseDialogListener {
		public void onComplete(Bundle values) {
			final String postId = values.getString("post_id");
			if (postId != null) {
				Toast toast = Toast.makeText(
						initActivity.getApplicationContext(),
						"Publié sur Facebook", Toast.LENGTH_SHORT);
				toast.show();
				// new UpdateStatusResultDialog(initActivity,
				// "Update Status executed", values).show();
			} else {
				Toast toast = Toast.makeText(
						initActivity.getApplicationContext(),
						"Le post n'a pas été mis sur le mur", Toast.LENGTH_SHORT);
				toast.show();
			}
		}

		public void onFacebookError(FacebookError error) {
			Toast.makeText(initActivity.getApplicationContext(),
					"Facebook Error: " + error.getMessage(), Toast.LENGTH_SHORT)
					.show();
		}

		public void onCancel() {
			Toast toast = Toast.makeText(initActivity.getApplicationContext(),
					"La mis à jour du statut a été annulée", Toast.LENGTH_SHORT);
			toast.show();
		}
	}

	/*
	 * Callback for fetching current user's name, picture, uid.
	 */
	public class UserRequestListener extends BaseRequestListener {

		public void onComplete(final String response, final Object state) {
		}
	}

	/*
	 * The Callback for notifying the application when authorization succeeds or
	 * fails.
	 */
	private final class LoginDialogListener implements DialogListener {
		public void onComplete(Bundle values) {
			SessionEvents.onLoginSuccess();
			Log.d("login", "success");

			sendStatus(caption, description, picture, name);
		}

		public void onFacebookError(FacebookError error) {
			SessionEvents.onLoginError(error.getMessage());
		}

		public void onError(DialogError error) {
			SessionEvents.onLoginError(error.getMessage());
		}

		public void onCancel() {
			SessionEvents.onLoginError("Action annulée");
		}
	}

}
