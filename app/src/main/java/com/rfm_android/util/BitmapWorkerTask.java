package com.rfm.android.util;

import java.io.IOException;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;


class BitmapWorkerTask extends AsyncTask<String, Void, Bitmap> {
	private int index;
	private ImageView image;
	private ListView listView;
	private Bitmap bitmapNews;
	private ProgressBar progress;


public BitmapWorkerTask(int position,
		ImageView imageview, ListView listview, ProgressBar progress) {
	index = position;
	image = imageview;
	this.listView = listview;
	this.progress = progress;
}

// Decode image in background.

@Override
protected Bitmap doInBackground(String... params) {
		try {
			bitmapNews = ImageFetcher.GetImageFromUrl(params[0]);
		} catch (IOException e) {
			e.printStackTrace();
		}
	if (bitmapNews != null) {
		ImageFetcher.addBitmapToMemoryCache(String.valueOf(params[0]), bitmapNews);
		return bitmapNews;
	} else {
		return null;
	}
}

@Override
protected void onPostExecute(Bitmap result) {
	super.onPostExecute(result);
	// if the current item is still visible, set bitmap
	if (result == null) {
		Log.v("onPostExecute", "null Bitmap");
	}

	View view = null;
	if (listView != null) {
		//check whether the item is displayed or not
		view = listView.getChildAt(index
				- listView.getFirstVisiblePosition());
		if (view != null) {
			image.setImageBitmap(result);
			image.setVisibility(View.VISIBLE);
			
			if (progress != null) {
				progress.setVisibility(View.GONE);
			}		
			Log.v("onPostExecute", "bitmap set");
		} else {
			Log.v("onPostExecute", "view null");
		}
	} else {
		image.setImageBitmap(result);
	}
	

}
}
