package com.rfm.android.util;

import java.io.IOException;
import java.net.URL;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.util.LruCache;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.rfm.android.R;

public class ImageFetcher {
	private static LruCache<String, Bitmap> mMemoryCache;
	
	public static void InitCache(){
		// Get max available VM memory, exceeding this amount will throw an
		// OutOfMemory exception. Stored in kilobytes as LruCache takes an
		// int in its constructor.
		final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);

		// Use 1/8th of the available memory for this memory cache.
		final int cacheSize = maxMemory / 8;

		mMemoryCache = new LruCache<String, Bitmap>(cacheSize) {
			@Override
			protected int sizeOf(String key, Bitmap value) {
				if (Integer.valueOf(android.os.Build.VERSION.SDK_INT) >= 12)
					return value.getByteCount();
				else
					return (value.getRowBytes() * value.getHeight());
			}
		};		
	}
	

	public static Bitmap GetImageFromUrl(String picture) throws IOException {

		URL url;
		Bitmap bitmap = null;
		Log.v("PictureURL", picture);

		url = new URL(picture);

		bitmap = BitmapFactory.decodeStream(url.openConnection()
				.getInputStream());

		if (bitmap != null && bitmap.getWidth() > 2000) {
			return Bitmap.createScaledBitmap(bitmap, bitmap.getWidth() / 4,
					bitmap.getHeight() / 4, true);
		}
		return bitmap;
	}

	//Image caching
	public static void addBitmapToMemoryCache(String key, Bitmap bitmap) {
		if (getBitmapFromMemCache(key) == null) {
			mMemoryCache.put(key, bitmap);
		}
	}

	private static Bitmap getBitmapFromMemCache(String key) {
		return mMemoryCache.get(key);
	}

	public static void loadBitmap(String resId, ImageView imageView, int position, ListView listView, ProgressBar progress) {

		final Bitmap bitmap = getBitmapFromMemCache(resId);
		Log.v("LoadBitmap", resId);
		if (bitmap != null) {
			imageView.setImageBitmap(bitmap);
			progress.setVisibility(View.GONE);
		} else {
			if (resId.equals("")) {;
				imageView.setImageResource(R.drawable.ic_station_1);
				progress.setVisibility(View.GONE);

			} else {
				BitmapWorkerTask task = new BitmapWorkerTask(position, imageView, listView, progress);
				task.execute(resId);
			}
		}
	}
}






