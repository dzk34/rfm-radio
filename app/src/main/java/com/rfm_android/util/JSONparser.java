package com.rfm.android.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Base64;
import android.util.Log;

import com.rfm.android.ApplicationData;
import com.rfm.android.model.AppMainModel;
import com.rfm.android.model.Radio;
import com.rfm.android.model.Share;
import com.rfm.android.model.SongMetadata;

public class JSONparser {
	private static final String METADATA_URL = "http://metaphor.goomradio.com/now/{id_radio}";
	//"http://metaphor.goomradio.com/now/{id_radio}/limit/2?whitelist=1";
	
	private static final String VERSION = "latest_version";
	private static final String RADIOS = "radios";
	//private static final String MODE = "mode";
	private static final String LIVE_URL = "live_url";
	private static final String LOGO = "logo";
	private static final String NAME = "name";
	private static final String SHARE = "share";
	private static final String FACEBOOK_URL = "facebook_url";
	private static final String TWEET = "tweet";
	private static final String MAIL_SUBJECT = "mail_subject";
	private static final String MAIL_BODY = "mail_body";
	private static final String PODCAST_URL = "podcast_url";
	private static final String PODCAST_SHARE = "podcast_share";
	private static final String PROGRAM_URL = "program_url";
	private static final String EVENTS_URL = "events_url";
	private static final String LIVE_UID = "live_uid";
	
	private static final String TITLE = "title";
	private static final String COVER_URL = "cover_url";
	private static final String ARTIST = "artist";
	private static final String RESULTS = "results";
	private static final String RID = "rid";
	private static final String FLUX = "flux";
	private static final String CITY = "city";
	private static final String FREQ = "freq";


	public static JSONObject GetJSONGetMethod(String requete)
			throws ClientProtocolException, IOException{

		// get httpResponse
		HttpClient httpClient = new DefaultHttpClient();
		HttpGet httpGet = new HttpGet(requete);
		HttpResponse response = null;
		response = httpClient.execute(httpGet);
		Log.v("getJSON", "request executed");

		// convert response to string
		String json_string;
		BufferedReader reader = null;
		JSONObject json = null;
		InputStream in = response.getEntity().getContent();
		reader = new BufferedReader(new InputStreamReader(in));

		String line = "";
		json_string = "";
		while ((line = reader.readLine()) != null) {
			json_string += line;
		}
		Log.d("Json string", json_string);

		try {
			json = new JSONObject(json_string);
		} catch (JSONException e1) {
			e1.printStackTrace();
			json = new JSONObject();			
		}


		if (reader != null) {
			try {
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return json;
	}

	public static JSONObject GetJSONAuthenticatedRequest(String url, String latitude, String longitude) throws Exception{
		HttpClient client = new DefaultHttpClient();

		HttpGet http = new HttpGet(url + latitude+ "&long=" +longitude);
		http.setHeader("Authorization", "Basic " + new String(Base64.encode("appsolute:vFc8R7yo".getBytes(), Base64.NO_WRAP)));

		HttpResponse resp;
		try {
			resp = client.execute(http);
			
			InputStream content = resp.getEntity().getContent();
			BufferedReader reader = new BufferedReader(new InputStreamReader(content));
			StringBuilder total = new StringBuilder();
			String line;
			while ((line = reader.readLine()) != null) {
			    total.append(line);
			}
			
			return new JSONObject(total.toString());
		} catch (ClientProtocolException e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		} catch (JSONException e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}

	public static SongMetadata GetCurrentTrack(String radioId)
			throws JSONException, ClientProtocolException, IOException {

		String url = METADATA_URL.replace("{id_radio}", radioId);
		JSONObject json = null;
		json = GetJSONGetMethod(url);
		
		JSONArray results = json.getJSONArray(RESULTS);
		JSONObject object = results.getJSONObject(0);
		
		SongMetadata metadata = new SongMetadata();
		metadata.setTitle(object.getString(TITLE));
		metadata.setArtist(object.getString(ARTIST));
		metadata.setMediaCover(object.getString(COVER_URL));
		
		return metadata;
	}
	
	public static Radio parseLiveFeed(JSONObject rootObject){
		Radio radio = new Radio();
		
		try{
			radio.setName("RFM LIVE");
			radio.setRadioId(rootObject.getString(RID));
			radio.setLive_url(rootObject.getString(FLUX));
			radio.setFreq(rootObject.getString(FREQ));
			radio.setCity(rootObject.getString(CITY));
		}
		catch(Exception e){
			radio = null;
		}
		
		return radio;
	}
	
	public static AppMainModel parseMainJSON(JSONObject rootObject){
		AppMainModel model = ApplicationData.getMainModel();
		
		try {
			model.setVersion(rootObject.getString(VERSION));
			model.setPodcast_url(rootObject.getString(PODCAST_URL));
			model.setProgram_url(rootObject.getString(PROGRAM_URL));
			model.setEvents_url(rootObject.getString(EVENTS_URL));
			
			JSONArray radiosArray = rootObject.getJSONArray(RADIOS);
			
			JSONObject shareObj;
			Share share;
			Radio radio;
			for (int i = 0; i < radiosArray.length(); i++) {
				shareObj = radiosArray.getJSONObject(i).getJSONObject(SHARE);
				
				share = new Share(shareObj.getString(FACEBOOK_URL), 
						shareObj.getString(TWEET), 
						shareObj.getString(MAIL_SUBJECT),
						shareObj.getString(MAIL_BODY));
				
				radio = new Radio(
						radiosArray.getJSONObject(i).getString(LIVE_UID),
						radiosArray.getJSONObject(i).getString(NAME),
						radiosArray.getJSONObject(i).getString(LIVE_URL),
						radiosArray.getJSONObject(i).getString(LOGO),
						share);
				
				if (model.getRadios().size()<3){
					model.getRadios().add(0, radio);
				}
				break;
			}
			
			shareObj = rootObject.getJSONObject(PODCAST_SHARE);
			share = new Share(shareObj.getString(FACEBOOK_URL), 
					shareObj.getString(TWEET), 
					shareObj.getString(MAIL_SUBJECT),
					shareObj.getString(MAIL_BODY));
			
			model.setPodcast_share(share);
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return model;
	}
}

