package com.rfm.android.activity;

import java.util.Arrays;
import java.util.List;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.widget.FacebookDialog;
import com.rfm.android.ApplicationData;
import com.rfm.android.R;
import com.rfm.android.all.ResponseCode;
import com.rfm.android.model.Category;
import com.rfm.android.util.ImageFetcher;

public class PodcastListActivity extends BaseActivity {
	Activity mActivity;
	Context mContext;	
	ListView listview;
	TextView errorText, podcast_title, podcast_descr;
	ImageView img_podcast;
	Drawable mDefaultDrawable;
	Category category;
	Runnable runInUI;
	ActionBar actionBar;
	ResponseCode err = ResponseCode.OK;
	Bitmap bitmap;
	Handler handler = new Handler();
	
	private Session.StatusCallback statusCallback = new SessionStatusCallback();
	private UiLifecycleHelper uiHelper;
	private Fragment fragmentContext;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_list);

		uiHelper = new UiLifecycleHelper(this, statusCallback);
		uiHelper.onCreate(savedInstanceState);
		
		setActionBar();		
		
		mActivity = this;
		mContext = this;
		
		listview = (ListView)findViewById(R.id.listview);
		errorText = (TextView)findViewById(R.id.errorText);
		podcast_descr = (TextView)findViewById(R.id.podcast_descr);
		podcast_title = (TextView) findViewById(R.id.podcast_title);
		img_podcast = (ImageView) findViewById(R.id.podcast_picture);

		category = getIntent().getExtras().getParcelable("category");
		
		listview.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,	int position, long arg3) {				
				Intent i = new Intent(PodcastListActivity.this, PodcastDetailActivity.class);
				
				i.putExtra("category", category);
				i.putExtra("positionPodcast", position);
				startActivity(i);
			}
		});
		
		if(category.getPodcasts().size()==0) {
			listview.setVisibility(View.GONE);
			errorText.setVisibility(View.VISIBLE);
		} else {
			listview.setVisibility(View.VISIBLE);
			errorText.setVisibility(View.GONE);
			
			podcast_title.setText(category.getTitle());
			podcast_title.setTypeface(ApplicationData.getTypeFaceRegular(getAssets()));
			podcast_descr.setText(category.getDescr());
			podcast_descr.setTypeface(ApplicationData.getTypeFaceRegular(getAssets()));
			ImageFetcher.loadBitmap(category.getImg(), img_podcast, 0, null, null);
			
			listview.setAdapter(new ASyncAdapter(mActivity));
		}
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) { 
	        switch (item.getItemId()) {
	        case android.R.id.home: 
	            onBackPressed();
	            return true;
	        }

	    return super.onOptionsItemSelected(item);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		getSupportMenuInflater().inflate(R.menu.share_menu, menu);
		return true;
	}

	
	public static class ViewHolder {
		TextView mTitleView;
		TextView mDateView;
	}
	
	public class ASyncAdapter extends BaseAdapter {
		Activity activity;
		
		public ASyncAdapter(Activity activity) {
			this.activity = activity;
			mDefaultDrawable = getResources().getDrawable(R.drawable.ic_launcher);
		}
		
		public int getCount() {
			return category.getPodcasts().size();
		}
	
		public Object getItem(int position) {			
			return null;
		}
	
		public long getItemId(int position) {
			return position;
		}
	
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder = new ViewHolder();
			convertView = getLayoutInflater().inflate(R.layout.podcast_item, parent, false);
			try {	
				holder.mTitleView = (TextView) convertView.findViewById(R.id.item_title);
				holder.mDateView = (TextView) convertView.findViewById(R.id.item_date);
				convertView.setTag(holder);	
								
				holder.mTitleView.setText(category.getPodcasts().get(position).getTitle());
				
				String date  = category.getPodcasts().get(position).getDate();						
				
				holder.mDateView.setText(date);				
				holder.mTitleView.setTypeface(ApplicationData.getTypeFaceRegular(getAssets()));
				holder.mDateView.setTypeface(ApplicationData.getTypeFaceRegular(getAssets()));
			} catch (Exception e) {
				e.printStackTrace();
			}									
			return convertView;
		}			
	}
	
	
	public void ShareOnFacebook (MenuItem item){

//		facebookActivity = new FacebookShare(
//				mActivity, "", 
//				"", 
//				ApplicationData.getMainModel().getPodcast_share().getFacebook_url(), 
//				ApplicationData.getMainModel().getPodcast_share().getMail_subject()
//				);
//		facebookActivity.init();
//		facebookActivity.init();
		Session.openActiveSession(this, true, statusCallback);
	}


	public void ShareOnTwitter (MenuItem item){
		Intent tweetIntent = new Intent(Intent.ACTION_SEND);
		tweetIntent.setType("text/plain");
		if (setTwitterPackage(tweetIntent)) {
			tweetIntent.putExtra(Intent.EXTRA_TEXT, ApplicationData.getMainModel().getPodcast_share().getTweetPodcast(category.getTitle(), category.getPodcasts().get(0).getTitle()));
			startActivity(tweetIntent);
		} else {
			try {
				tweetIntent.setAction(Intent.ACTION_VIEW);
				tweetIntent.setData(Uri.parse("market://details?id="
						+ "com.twitter.android"));
				startActivity(tweetIntent);
			} catch (ActivityNotFoundException e) {
				// in case device has not Google Play anymore ...
				Toast.makeText(mActivity,
						mActivity.getText(R.string.install_twitter),
						Toast.LENGTH_SHORT).show();
			}
		}
	}
	
	protected boolean setTwitterPackage(Intent tweetIntent) {
		// best-known twitter package names
		final String[] twitterPackage = { "com.twitter.android",
				"com.twidroid", "com.handmark.tweetcaster",
				"com.thedeck.android" };
		final PackageManager packageManager = mActivity.getPackageManager();
		List<ResolveInfo> list = packageManager.queryIntentActivities(
				tweetIntent, PackageManager.MATCH_DEFAULT_ONLY);
		for (ResolveInfo resolveInfo : list) {
			String p = resolveInfo.activityInfo.packageName;
			for (int i = 0; i < twitterPackage.length; i++) {
				if (p != null && p.startsWith(twitterPackage[i])) {
					tweetIntent.setPackage(p);
					return true;
				}
			}
		}
		return false;
	}
	
	private class SessionStatusCallback implements Session.StatusCallback {

		@Override
		public void call(Session session, SessionState state,
				Exception exception) {
			if (session.isOpened()){

				Session.NewPermissionsRequest newPermissionsRequest = new Session
						.NewPermissionsRequest(fragmentContext, Arrays.asList("publish_actions"));
				session.requestNewPublishPermissions(newPermissionsRequest);

				openShareDialog();
			}	

			if (state == SessionState.CLOSED_LOGIN_FAILED){
				Toast.makeText(mActivity,getString(R.string.facebook_cannot_login), Toast.LENGTH_SHORT).show();
			}
		}
	}
	
	private void openShareDialog () {
	
		
        FacebookDialog.ShareDialogBuilder builder = new FacebookDialog.ShareDialogBuilder(this)
		.setLink(ApplicationData.getMainModel().getPodcast_share().getFacebook_url())
		.setPicture(getString(R.string.logo_rfm_live))
		.setName("RFM Podcasts")
		.setDescription(ApplicationData.getMainModel().getPodcast_share().getMail_subject());
        

		// share the app
		if (builder.canPresent()) {
		    builder.build().present();
		}
	}
	
}