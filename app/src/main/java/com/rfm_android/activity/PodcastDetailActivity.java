package com.rfm.android.activity;

import java.util.Arrays;
import java.util.List;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.MediaController.MediaPlayerControl;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.widget.FacebookDialog;
import com.rfm.android.ApplicationData;
import com.rfm.android.R;
import com.rfm.android.mediaplayer.BackgroundMediaPlayer;
import com.rfm.android.mediaplayer.MediaAction;
import com.rfm.android.model.Category;
import com.rfm.android.model.Podcast;
import com.rfm.android.util.ImageFetcher;

public class PodcastDetailActivity extends BaseActivity implements MediaPlayerControl {
	Context mContext;
	TextView datePodcast, titlePodcast, descrPodcast, titleCategory, descrCategory;
	ImageView imgView;
	ImageButton play;
	View layout;
	ActionBar actionBar;
	private boolean isPlaying = false;
	

	private Session.StatusCallback statusCallback = new SessionStatusCallback();
	private UiLifecycleHelper uiHelper;
	private Fragment fragmentContext;
	
	private BackgroundMediaPlayer backgroundPlayer;
	
	private Category category;
	private Podcast podcast;
	
	private ServiceConnection mConnection = new ServiceConnection() {

	    public void onServiceConnected(ComponentName className, IBinder binder) {
	    	backgroundPlayer = ((BackgroundMediaPlayer.MyBinder) binder).getBackgroundPlayer();
	    }

	    public void onServiceDisconnected(ComponentName className) {
	    	backgroundPlayer = null;
	    }
	  };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_detail);
		
		mContext = this;
		uiHelper = new UiLifecycleHelper(this, statusCallback);
		uiHelper.onCreate(savedInstanceState);
		
		setActionBar();
		
		play = (ImageButton) findViewById(R.id.btn_play_podcast);
		datePodcast = (TextView)findViewById(R.id.dateText);
		titlePodcast = (TextView)findViewById(R.id.titleText);
		descrPodcast = (TextView)findViewById(R.id.descrText);
		titleCategory = (TextView)findViewById(R.id.category_title);
		descrCategory = (TextView)findViewById(R.id.category_descr);
		
		imgView = (ImageView) findViewById(R.id.img_podcast_detail);
		layout =  findViewById(R.id.podcast_detail_layout);
		
		titleCategory.setTypeface(ApplicationData.getTypeFaceRegular(getAssets()));
		descrCategory.setTypeface(ApplicationData.getTypeFaceRegular(getAssets()));
		datePodcast.setTypeface(ApplicationData.getTypeFaceRegular(getAssets()));
		titlePodcast.setTypeface(ApplicationData.getTypeFaceRegular(getAssets()));
		descrPodcast.setTypeface(ApplicationData.getTypeFaceRegular(getAssets()));
		
		category = getIntent().getExtras().getParcelable("category");
		int position = getIntent().getIntExtra("positionPodcast", 0);
		podcast = category.getPodcasts().get(position);
		
		initDetails();
		
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		getSupportMenuInflater().inflate(R.menu.share_menu, menu);
		return true;
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		bindService(new Intent(getApplicationContext(), BackgroundMediaPlayer.class), mConnection, Context.BIND_AUTO_CREATE);
	}
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		unbindService(mConnection);
	}
	
	private void initDetails(){
		if(!podcast.getTitle().equals("") && !podcast.getTitle().equals(null))
			titlePodcast.setText(podcast.getTitle());
		else
			titlePodcast.setVisibility(View.GONE);
		
		if(!podcast.getDate().equals("") && !podcast.getDate().equals(null))
			datePodcast.setText(podcast.getDate());
		else
			datePodcast.setVisibility(View.GONE);
				
		if(!podcast.getDescription().equals("") && !podcast.getDescription().equals(null))
			descrPodcast.setText(podcast.getDescription());
		else
			descrPodcast.setVisibility(View.GONE);
		
		if(!category.getDescr().equals("") && !category.getDescr().equals(null))
			descrCategory.setText(category.getDescr());
		else
			descrCategory.setVisibility(View.GONE);
		
		if(!category.getTitle().equals("") && !category.getTitle().equals(null))
			titleCategory.setText(category.getTitle());
		else
			titleCategory.setVisibility(View.GONE);
		
		if(!podcast.getThumb().equals("") && !podcast.getThumb().equals(null))
			ImageFetcher.loadBitmap(podcast.getThumb(), imgView, 0, null, null);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) { 
	        switch (item.getItemId()) {
	        case android.R.id.home: 
	            onBackPressed();
	            return true;
	        }

	    return super.onOptionsItemSelected(item);
	}
	
	
	
	public void ShareOnFacebook (MenuItem item){

		
		Session.openActiveSession(this, true, statusCallback);
	}


	public void ShareOnTwitter (MenuItem item){
		Intent tweetIntent = new Intent(Intent.ACTION_SEND);
		tweetIntent.setType("text/plain");
		if (setTwitterPackage(tweetIntent)) {
			tweetIntent.putExtra(Intent.EXTRA_TEXT, ApplicationData.getMainModel().getPodcast_share().getTweetPodcast(category.getTitle(),podcast.getTitle()));
			startActivity(tweetIntent);
		} else {
			try {
				tweetIntent.setAction(Intent.ACTION_VIEW);
				tweetIntent.setData(Uri.parse("market://details?id="
						+ "com.twitter.android"));
				startActivity(tweetIntent);
			} catch (ActivityNotFoundException e) {
				// in case device has not Google Play anymore ...
				Toast.makeText(this,
						this.getText(R.string.install_twitter),
						Toast.LENGTH_SHORT).show();
			}
		}
	}
	
	protected boolean setTwitterPackage(Intent tweetIntent) {
		// best-known twitter package names
		final String[] twitterPackage = { "com.twitter.android",
				"com.twidroid", "com.handmark.tweetcaster",
				"com.thedeck.android" };
		final PackageManager packageManager = this.getPackageManager();
		List<ResolveInfo> list = packageManager.queryIntentActivities(
				tweetIntent, PackageManager.MATCH_DEFAULT_ONLY);
		for (ResolveInfo resolveInfo : list) {
			String p = resolveInfo.activityInfo.packageName;
			for (int i = 0; i < twitterPackage.length; i++) {
				if (p != null && p.startsWith(twitterPackage[i])) {
					tweetIntent.setPackage(p);
					return true;
				}
			}
		}
		return false;
	}
	
	public void onPlayerClick (View button){
		if (isPlaying){
			isPlaying = false;
			pause();
		} else {
			play();
			isPlaying = true;
		}
	}

	
	public void play(){
		bindMediaController();
		play.setImageResource(R.drawable.pause);
		Intent intent = new Intent(this, BackgroundMediaPlayer.class);
		intent.setAction(MediaAction.PLAY_MP4.toString());
		intent.putExtra("url", podcast.getFilepath());
		startService(intent);
	}
	
	private void bindMediaController(){
		final MediaController controller = new MediaController(this) {

			private boolean end_activity = false;

			@Override
			public void hide() {
				// TODO Auto-generated method stub
				if (end_activity) {
					super.hide();
				} else {
					show();
				}
			}

			@Override
			public boolean dispatchKeyEvent(KeyEvent event) {
				// TODO Auto-generated method stub

				if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
					end_activity = true;
					hide();
					((Activity) getContext()).finish();
				}

				return super.dispatchKeyEvent(event);
			}
		};
		controller.setMediaPlayer(this);
		controller.setAnchorView(layout);

		Log.d("URL", podcast.getFilepath());
		
		controller.show();
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		if(backgroundPlayer != null){
			backgroundPlayer.stopMP4Player(true);
		}
	}

	@Override
	public boolean canPause() {
		return true;
	}

	@Override
	public boolean canSeekBackward() {
		return true;
	}

	@Override
	public boolean canSeekForward() {
		return true;
	}

	@Override
	public int getBufferPercentage() {
		return backgroundPlayer.getBufferedPercent();
	}

	@Override
	public int getCurrentPosition() {
		return backgroundPlayer.getCurrentPosition();
	}

	@Override
	public int getDuration() {
		return backgroundPlayer.getDuration();
	}

	@Override
	public boolean isPlaying() {
		return backgroundPlayer.isPlaying();
	}

	@Override
	public void pause() {
		play.setImageResource(R.drawable.btn_play);
		backgroundPlayer.pauseMP4Player(true);
	}

	@Override
	public void seekTo(int pos) {
		backgroundPlayer.seekTo(pos);
	}

	@Override
	public void start() {
		backgroundPlayer.playMP4Player();
	}
	
	private class SessionStatusCallback implements Session.StatusCallback {

		@Override
		public void call(Session session, SessionState state,
				Exception exception) {
			if (session.isOpened()){

				Session.NewPermissionsRequest newPermissionsRequest = new Session
						.NewPermissionsRequest(fragmentContext, Arrays.asList("publish_actions"));
				session.requestNewPublishPermissions(newPermissionsRequest);

				openShareDialog();
			}	

			if (state == SessionState.CLOSED_LOGIN_FAILED){
				Toast.makeText(mContext,getString(R.string.facebook_cannot_login), Toast.LENGTH_SHORT).show();
			}
		}
	}
	
	private void openShareDialog () {
	
		
        FacebookDialog.ShareDialogBuilder builder = new FacebookDialog.ShareDialogBuilder(this)
		.setLink(ApplicationData.getMainModel().getPodcast_share().getFacebook_url())
		.setPicture(getString(R.string.logo_rfm_live))
		.setName("RFM Podcasts")
		.setDescription(ApplicationData.getMainModel().getPodcast_share().getMail_subject());
        

		// share the app
		if (builder.canPresent()) {
		    builder.build().present();
		}
	}

	@Override
	public int getAudioSessionId() {
		// TODO Auto-generated method stub
		return 0;
	}

}
