package com.rfm.android.activity;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.view.MenuItem;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookOperationCanceledException;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.widget.FacebookDialog;
import com.rfm.android.R;
import com.ubikod.capptain.android.sdk.CapptainAgent;
import com.ubikod.capptain.android.sdk.CapptainAgentUtils;

public class MainActivity extends CapptainFragmentActivity {
	public static Activity activity;

    private final String PENDING_ACTION_BUNDLE_KEY = "com.rfm.android.activity:PendingAction";

    private PendingAction pendingAction = PendingAction.NONE;

    private enum PendingAction {
        NONE,
        POST_PHOTO,
        POST_STATUS_UPDATE
    }
    private UiLifecycleHelper uiHelper;

    private Session.StatusCallback callback = new Session.StatusCallback() {
        @Override
        public void call(Session session, SessionState state, Exception exception) {
            onSessionStateChange(session, state, exception);
        }
    };

    private FacebookDialog.Callback dialogCallback = new FacebookDialog.Callback() {
        @Override
        public void onError(FacebookDialog.PendingCall pendingCall, Exception error, Bundle data) {
            Log.d("Facebook", String.format("Error: %s", error.toString()));
        }

        @Override
        public void onComplete(FacebookDialog.PendingCall pendingCall, Bundle data) {
            Log.d("Facebook", "Success!");
        }
    };
            
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

        uiHelper = new UiLifecycleHelper(this, callback);
        uiHelper.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		activity = this;	
		
		if (CapptainAgentUtils.isInDedicatedCapptainProcess(this))
		    return;
		CapptainAgent.getInstance(this).setEnabled(true);
		
		if (savedInstanceState != null) {
            String name = savedInstanceState.getString(PENDING_ACTION_BUNDLE_KEY);
            pendingAction = PendingAction.valueOf(name);
        }
		try {
            PackageInfo info = getPackageManager().getPackageInfo("com.rfm.android", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.e("MY KEY HASH:",
                        Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {
        	
        }
		
		//lock screen orientation
		boolean isTablet = (this.getResources().getConfiguration().screenLayout
				& Configuration.SCREENLAYOUT_SIZE_MASK)>= Configuration.SCREENLAYOUT_SIZE_LARGE;
		if (isTablet) { 
			setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		} else {
			setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		}
		
		// Hide status bar
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
		// Show status bar
		getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

		// --------------------- setting tabs indicator
		TabHost tabHost = (TabHost) findViewById(R.id.tabhost);
		tabHost.setup();

		// tab live
		TabSpec tab1 = tabHost.newTabSpec("TAB_LIVE");
		tab1.setIndicator(createTabView(this, R.drawable.ic_live_selector, "RADIO"));
		tab1.setContent(R.id.tab_live);
		tabHost.addTab(tab1);
		
		// tab live
		TabSpec tab3 = tabHost.newTabSpec("TAB_PODCAST");
		tab3.setIndicator(createTabView(this, R.drawable.ic_podcast_selector, "PODCASTS"));
		tab3.setContent(R.id.tab_podcast);
		tabHost.addTab(tab3);
		
		/*// tab live
		TabSpec tab4 = tabHost.newTabSpec("TAB_ACCOUNT");
		tab4.setIndicator(createTabView(this, R.drawable.ic_social_selector, "COMMUNAUTE"));
		tab4.setContent(R.id.tab_account);
		tabHost.addTab(tab4);*/

	}

	private static View createTabView(Context context, int resId, String title) {
		View view = LayoutInflater.from(context).inflate(R.layout.tab, null,
				false);
		((ImageView) view.findViewById(R.id.tabIcon)).setImageResource(resId);
		((TextView) view.findViewById(R.id.tabTitle)).setText(title);
		return view;
	}


	public void onClickShare(MenuItem menuItem) {
		Toast.makeText(this, "Button Share clicked", Toast.LENGTH_SHORT).show();
	}
			
	@Override
    protected void onResume() {
        super.onResume();
        String activityNameOnCapptain = CapptainAgentUtils.buildCapptainActivityName(getClass()); // Uses short class name and removes "Activity" at the end.
        Log.d("activityNameOnCapptain","==="+activityNameOnCapptain);
        CapptainAgent.getInstance(this).startActivity(this, activityNameOnCapptain, null);
        uiHelper.onResume();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        uiHelper.onSaveInstanceState(outState);

        outState.putString(PENDING_ACTION_BUNDLE_KEY, pendingAction.name());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        uiHelper.onActivityResult(requestCode, resultCode, data, dialogCallback);
    }

    @Override
    public void onPause() {
        super.onPause();
        CapptainAgent.getInstance(this).endActivity();
        uiHelper.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        uiHelper.onDestroy();
    }

    private void onSessionStateChange(Session session, SessionState state, Exception exception) {
        if (pendingAction != PendingAction.NONE &&
                (exception instanceof FacebookOperationCanceledException ||
                exception instanceof FacebookAuthorizationException)) {
                new AlertDialog.Builder(MainActivity.this)
                    .setTitle(R.string.cancelled)
                    .setMessage(R.string.permission_not_granted)
                    .setPositiveButton(R.string.ok, null)
                    .show();
            pendingAction = PendingAction.NONE;
        } else if (state == SessionState.OPENED_TOKEN_UPDATED) {
            handlePendingAction();
        }
    }
    
    @SuppressWarnings("incomplete-switch")
    private void handlePendingAction() {
        PendingAction previouslyPendingAction = pendingAction;
        // These actions may re-set pendingAction if they are still pending, but we assume they
        // will succeed.
        pendingAction = PendingAction.NONE;

        switch (previouslyPendingAction) {
            case POST_PHOTO:
                break;
        }
    }    

}
