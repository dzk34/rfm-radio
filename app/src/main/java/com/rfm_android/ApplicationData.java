package com.rfm.android;

import java.util.ArrayList;

import android.content.res.AssetManager;
import android.graphics.Typeface;

import com.rfm.android.model.AppMainModel;
import com.rfm.android.model.Category;

public class ApplicationData {

	private static ArrayList<Category> categories;
	private static AppMainModel mainModel;
	public static boolean IS_CONNECTED_TO_INTERNET = true;
	public static boolean IS_LOCATION_SET = false;
	public static boolean IS_CACHE_DATA_AVAILABLE = false;
	
	private static Typeface regularFont, boldFont;
	
	public static ArrayList<Category> getCategories() {
		if(categories == null){
			categories = new ArrayList<Category>();
		}
		return categories;
	}
	
	public static void setCategories(ArrayList<Category> cat){
		categories = cat;
	}
	
	public static AppMainModel getMainModel(){
		if(mainModel == null){
			mainModel = new AppMainModel();
		}
		return mainModel;
	}
	
	public static void setMainModel(AppMainModel model){
		mainModel = model;
	}
	
	public static Typeface getTypeFaceRegular(AssetManager am){
		if(regularFont == null){
			regularFont = Typeface.createFromAsset(am, "Hermes-Regular.ttf");
		}
		return regularFont;
	}
	
	public static Typeface getTypeFaceBold(AssetManager am){
		if(boldFont == null){
			boldFont = Typeface.createFromAsset(am, "Hermes-Bold.ttf");
		}
		return boldFont;
	}
}
