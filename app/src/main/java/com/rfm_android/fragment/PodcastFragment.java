package com.rfm.android.fragment;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.rfm.android.ApplicationData;
import com.rfm.android.R;
import com.rfm.android.activity.PodcastListActivity;
import com.rfm.android.model.Category;
import com.rfm.android.util.ImageFetcher;
import com.ubikod.capptain.android.sdk.CapptainAgent;
import com.ubikod.capptain.android.sdk.CapptainAgentUtils;

public class PodcastFragment extends Fragment {
	private View view;
	Activity mActivity;
	Context mContext;	
	ListView listview;
	TextView errorText;
	ArrayList<Category> arraylist;
	String responce;
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		super.onCreateView(inflater, container, savedInstanceState);		
		view = inflater.inflate(R.layout.fragment_podcast, container, false);
		
		listview = (ListView)view.findViewById(R.id.listview);
		errorText = (TextView)view.findViewById(R.id.errorText);
		errorText.setTypeface(ApplicationData.getTypeFaceRegular(getActivity().getAssets()));
		
		if (ApplicationData.IS_CACHE_DATA_AVAILABLE) {
			arraylist = ApplicationData.getCategories();
	        
			listview.setAdapter(new ASyncAdapter());
			listview.setOnItemClickListener(new OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,	int position, long arg3) {
					Intent i = new Intent(getActivity().getApplicationContext(), PodcastListActivity.class);
					i.putExtra("category", arraylist.get(position));
					startActivity(i);
				}
			});	
			
			if (arraylist.size() == 0){
				errorText.setVisibility(View.VISIBLE);
			}
			
		} else {
			errorText.setText("Une connexion à internet est requise");
			errorText.setVisibility(View.VISIBLE);
		}
		
		return view;
	}
	
	
		
	public static class ViewHolder {
		TextView mTitleView, mDescView;
		ImageView mThumbView;
		ProgressBar mProgressView;
	}
	
	public class ASyncAdapter extends BaseAdapter {		
		private ViewHolder holder;

		public int getCount() {
			return arraylist.size();
		}
	
		public Object getItem(int position) {			
			return null;
		}
	
		public long getItemId(int position) {
			return position;
		}
	
		public View getView(int position, View convertView, ViewGroup parent) {
			
			if (convertView == null) {
				holder = new ViewHolder();
				convertView = getActivity().getLayoutInflater().inflate(R.layout.category_item, parent, false);
				holder.mTitleView = (TextView) convertView.findViewById(R.id.item_title);
				holder.mDescView = (TextView) convertView.findViewById(R.id.item_descr);
				holder.mThumbView = (ImageView) convertView.findViewById(R.id.item_img);
				holder.mProgressView = (ProgressBar) convertView.findViewById(R.id.progress_category);
				convertView.setTag(holder);	
			} else {
				holder = (ViewHolder) convertView.getTag();
				holder.mProgressView.setVisibility(View.VISIBLE);
				holder.mThumbView.setVisibility(View.INVISIBLE);
			}
			
			holder.mTitleView.setText(arraylist.get(position).getTitle());
			holder.mTitleView.setTypeface(ApplicationData.getTypeFaceBold(getActivity().getAssets()));
			holder.mDescView.setText(arraylist.get(position).getDescr());
			holder.mDescView.setTypeface(ApplicationData.getTypeFaceRegular(getActivity().getAssets()));
			ImageFetcher.loadBitmap(arraylist.get(position).getImg(), holder.mThumbView, position, listview, holder.mProgressView);
								
			return convertView;
		}			
	}
	
	@Override
    public void onPause() {
        super.onPause();
        //CapptainAgent.getInstance(getActivity()).endActivity();
    }
	
	@Override
	public void onResume() {
        super.onResume();
       /* String activityNameOnCapptain = CapptainAgentUtils.buildCapptainActivityName(getClass()); // Uses short class name and removes "Activity" at the end.
        Log.d("activityNameOnCapptain","==="+activityNameOnCapptain);
        CapptainAgent.getInstance(getActivity()).startActivity(getActivity(), activityNameOnCapptain, null);*/
    }

}

