package com.rfm.android.fragment;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookException;
import com.facebook.FacebookOperationCanceledException;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.facebook.widget.FacebookDialog;
import com.facebook.widget.LoginImage;
import com.facebook.widget.WebDialog;
import com.facebook.widget.WebDialog.OnCompleteListener;
import com.rfm.android.ApplicationData;
import com.rfm.android.R;
import com.rfm.android.all.ResponseCode;
import com.rfm.android.mediaplayer.BackgroundMediaPlayer;
import com.rfm.android.mediaplayer.MediaAction;
import com.rfm.android.model.Radio;
import com.rfm.android.model.SongMetadata;
import com.rfm.android.util.ImageFetcher;
import com.rfm.android.util.JSONparser;
import com.ubikod.capptain.android.sdk.CapptainAgent;
import com.ubikod.capptain.android.sdk.CapptainAgentUtils;

public class LiveFragment extends Fragment {
	public final String SELECTED_RADIO = "selectedRadio";
	private View view;
	private Bitmap bitmap;
	private Handler handlerToSetPicture = new Handler();
	private Handler handlerOnUI = new Handler();
	private Thread updateButtonEnabledThread;
	private ImageView imgLive, shadow;
	private boolean isPlaying = false;
	private ImageView imgRfm3;
	private ImageView imgRfm2;
	private ImageView imgRfm1;
	private ImageView imgTwitter;
	private LoginImage imgFb;
	private TextView radioTitle, radioSubtitle, cityFreq;
	private ImageButton btn_player;
	private String lastPicture = null;
	private Runnable runInUI;
	private Runnable timer;
	private Runnable getPictureUrl;
	ResponseCode err = ResponseCode.OK;
	private final int timerPeriod = 20;
	public static boolean RADIO_GEOLOCALIZED;

	private ArrayList<Radio> radios;
	private static int selectedRadio;
	Activity activity;
	private SongMetadata currentSongMetadata = new SongMetadata();
	static Context context;
	SharedPreferences sharedPref;
	boolean flag = false;	

	///Facebook
	private static final String PERMISSION = "publish_actions";
    private final String PENDING_ACTION_BUNDLE_KEY = "com.rfm.android.activity:PendingAction";

    private PendingAction pendingAction = PendingAction.NONE;

    private enum PendingAction {
        NONE,
        POST_PHOTO,
        POST_STATUS_UPDATE
    }
    private UiLifecycleHelper uiHelper;

    private Session.StatusCallback callback = new Session.StatusCallback() {
        @Override
        public void call(Session session, SessionState state, Exception exception) {
            onSessionStateChange(session, state, exception);
        }
    };

    private FacebookDialog.Callback dialogCallback = new FacebookDialog.Callback() {
        @Override
        public void onError(FacebookDialog.PendingCall pendingCall, Exception error, Bundle data) {
            Log.d("RFM", String.format("Error: %s", error.toString()));
        }

        @Override
        public void onComplete(FacebookDialog.PendingCall pendingCall, Bundle data) {
            Log.d("RFM", "Success!");
        }
    };
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		activity = super.getActivity();
		uiHelper = new UiLifecycleHelper(activity, callback);
        uiHelper.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            String name = savedInstanceState.getString(PENDING_ACTION_BUNDLE_KEY);
            pendingAction = PendingAction.valueOf(name);
        }        
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		super.onCreateView(inflater, container, savedInstanceState);

		//restore previously selected radio
		sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
		selectedRadio = sharedPref.getInt(SELECTED_RADIO, 0);

		RADIO_GEOLOCALIZED = ApplicationData.IS_LOCATION_SET;
		view = inflater.inflate(R.layout.fragment_live, container, false);
		imgLive = (ImageView) view.findViewById(R.id.img_track);
		imgLive.setImageDrawable(getResources().getDrawable(R.drawable.logo_1));
		shadow = (ImageView) view.findViewById(R.id.img_shadow);
		radios = ApplicationData.getMainModel().getRadios();

		initRunnable();
		context = getActivity().getApplicationContext();
		// set onClickListeners
		imgTwitter = (ImageView) view.findViewById(R.id.img_twitter);
		imgFb = (LoginImage) view.findViewById(R.id.img_fb);
		
		OnClickListener share = new OnClickListener() {
			@Override
			public void onClick(View v) {
				switch (v.getId()) {				
				case R.id.img_twitter:
					startTwitterActivity();
					/*
					 * Toast.makeText(getSherlockActivity(), "Share on Twitter",
					 * Toast.LENGTH_SHORT).show();
					 */
					break;
				}
			}
		};
		imgTwitter.setOnClickListener(share);
		//imgFb.setOnClickListener(share);
		imgFb.setUserInfoChangedCallback(new LoginImage.UserInfoChangedCallback() {
            @Override
            public void onUserInfoFetched(GraphUser user) {
                // It's possible that we were waiting for this.user to be populated in order to post a
                // status update.
                handlePendingAction();   
                Session session = Session.getActiveSession();
                boolean enableButtons = (session != null && session.isOpened());
                if(!enableButtons)
                	flag = true;	
                if(enableButtons && flag) {
                	onClickPostPhoto();
                }
                flag = true;	
            }
        });

		// set play/pause button
		btn_player = (ImageButton) view.findViewById(R.id.btn_play);
		OnClickListener playerClick = new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (isPlaying) {
					pauseLive();
				} else {
					playLive();
				}
			}
		};
		btn_player.setOnClickListener(playerClick);

		// set selected radio
		imgRfm1 = (ImageView) view.findViewById(R.id.img_rfm_1);
		imgRfm2 = (ImageView) view.findViewById(R.id.img_rfm_2);
		imgRfm3 = (ImageView) view.findViewById(R.id.img_rfm_3);
		radioTitle = (TextView) view.findViewById(R.id.tv_radio_title);
		radioSubtitle = (TextView) view.findViewById(R.id.tv_radio_subtitle);
		cityFreq = (TextView) view.findViewById(R.id.tv_radio_city);
		cityFreq.setTypeface(ApplicationData.getTypeFaceRegular(getActivity()
				.getAssets()));
		radioTitle.setTypeface(ApplicationData.getTypeFaceRegular(getActivity()
				.getAssets()));
		radioSubtitle.setTypeface(ApplicationData
				.getTypeFaceRegular(getActivity().getAssets()));

		TextView titleRadios = (TextView) view.findViewById(R.id.tv_rfm_radios);
		if (titleRadios != null) {
			titleRadios.setTypeface(ApplicationData
					.getTypeFaceRegular(getActivity().getAssets()));
		}

		if (radios.get(selectedRadio)!= null && radios.get(selectedRadio).getFreq() !=null){
			cityFreq.setText(radios.get(selectedRadio).getFreq()+" - "+radios.get(selectedRadio).getCity());
		} else {
			cityFreq.setVisibility(View.INVISIBLE);
		}

		OnClickListener changeRadio = new OnClickListener() {
			@Override
			public void onClick(View v) {
				//if the location wasn't set after the splash, check if current location is set
				//If so, reload live feed

				shadow.setVisibility(View.INVISIBLE);
				currentSongMetadata = new SongMetadata("", "", "");

				if (RADIO_GEOLOCALIZED && radios.get(0)!=null){
					imgRfm1.setVisibility(View.VISIBLE);
				}

				switch (v.getId()) {
				case R.id.img_rfm_1:
					selectedRadio = 0;
					pauseLive();

					if(radios.get(selectedRadio) != null){
						if (RADIO_GEOLOCALIZED == true && radios.get(selectedRadio).getFreq() !=null){
							cityFreq.setText(radios.get(selectedRadio).getFreq()+" - "+radios.get(selectedRadio).getCity());
							cityFreq.setVisibility(View.VISIBLE);
						}
						imgRfm1.setSelected(true);
						imgRfm2.setSelected(false);
						imgRfm3.setSelected(false);
						radioTitle.setText(radios.get(selectedRadio).getName());
						radioSubtitle.setText("");
						imgLive.setImageDrawable(getResources().getDrawable(R.drawable.logo_1));
					} else {						
						imgRfm1.setVisibility(View.GONE);
						onClick(imgRfm2);
					}
					break;

				case R.id.img_rfm_2:
					selectedRadio = 1;
					pauseLive();

					cityFreq.setVisibility(View.INVISIBLE);
					imgRfm1.setSelected(false);
					imgRfm2.setSelected(true);
					imgRfm3.setSelected(false);
					radioTitle.setText(radios.get(selectedRadio).getName());
					radioSubtitle.setText("");
					imgLive.setImageDrawable(getResources().getDrawable(R.drawable.logo_2));
					break;

				case R.id.img_rfm_3:
					selectedRadio = 2;
					pauseLive();

					cityFreq.setVisibility(View.INVISIBLE);

					imgRfm1.setSelected(false);
					imgRfm2.setSelected(false);
					imgRfm3.setSelected(true);
					radioTitle.setText(radios.get(selectedRadio).getName());
					radioSubtitle.setText("");
					imgLive.setImageDrawable(getResources().getDrawable(R.drawable.logo_3));
					break;
				}
			}
		};

		imgRfm1.setOnClickListener(changeRadio);
		imgRfm2.setOnClickListener(changeRadio);
		imgRfm3.setOnClickListener(changeRadio);

		// set default radio
		switch (selectedRadio){
		case 0 : 
			changeRadio.onClick(imgRfm1);
			break;
		case 1 : 
			changeRadio.onClick(imgRfm2);
			break;
		case 2 :
			changeRadio.onClick(imgRfm3);
			break;
		}	;


		// set timer
		timer = new Runnable() {

			@Override
			public void run() {
				try {
					while (isPlaying) {
						new Thread(getPictureUrl).start();
						Thread.sleep(timerPeriod * 1000);
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		};

		updateButtonEnabledThread = new Thread(new Runnable() {
			@Override
			public void run() {
				btn_player.setEnabled(true);
			}
		});

		playLive();
		return view;
	}
	
	private void startTwitterActivity() {
		Intent tweetIntent = new Intent(Intent.ACTION_SEND);
		tweetIntent.setType("text/plain");
		if (setTwitterPackage(tweetIntent)) {
			if (isPlaying && currentSongMetadata.getArtist() != null){
				tweetIntent.putExtra(Intent.EXTRA_TEXT, radios.get(selectedRadio).getShare().getTweet(currentSongMetadata.getArtist(),currentSongMetadata.getTitle()));
			} else {
				tweetIntent.putExtra(Intent.EXTRA_TEXT, "now playing sur @RFMFrance");
			}
			startActivity(tweetIntent);
		} else {
			try {
				tweetIntent.setAction(Intent.ACTION_VIEW);
				tweetIntent.setData(Uri.parse("market://details?id="
						+ "com.twitter.android"));
				startActivity(tweetIntent);
			} catch (ActivityNotFoundException e) {
				// in case device has not Google Play anymore ...
				Toast.makeText(LiveFragment.context,
						context.getText(R.string.install_twitter),
						Toast.LENGTH_SHORT).show();
			}
		}

	}

	protected boolean setTwitterPackage(Intent tweetIntent) {
		// best-known twitter package names
		final String[] twitterPackage = { "com.twitter.android",
				"com.twidroid", "com.handmark.tweetcaster",
		"com.thedeck.android" };
		final PackageManager packageManager = context.getPackageManager();
		List<ResolveInfo> list = packageManager.queryIntentActivities(
				tweetIntent, PackageManager.MATCH_DEFAULT_ONLY);
		for (ResolveInfo resolveInfo : list) {
			String p = resolveInfo.activityInfo.packageName;
			for (int i = 0; i < twitterPackage.length; i++) {
				if (p != null && p.startsWith(twitterPackage[i])) {
					tweetIntent.setPackage(p);
					return true;
				}
			}
		}
		return false;
	}

	private void pauseLive() {
		btn_player.setImageResource(R.drawable.btn_play);
		isPlaying = false;

		Intent intent = new Intent(getActivity(), BackgroundMediaPlayer.class);
		intent.setAction(MediaAction.PAUSE_ALL.toString());
		getActivity().startService(intent);
	}

	private void playLive() {
		if (ApplicationData.IS_CONNECTED_TO_INTERNET) {
			btn_player.setImageResource(R.drawable.pause);
			isPlaying = true;
			new Thread(timer).start();
			Intent intent = new Intent(getActivity(),
					BackgroundMediaPlayer.class);
			intent.setAction(MediaAction.PLAY_AAC.toString());
			intent.putExtra("url", radios.get(selectedRadio).getLive_url());
			intent.putExtra("receiver", new MediaLoadedReceiver(null));
			getActivity().startService(intent);
			btn_player.setEnabled(false);
		} else {
			Toast.makeText(getActivity(),
					getString(R.string.error_internet), Toast.LENGTH_SHORT)
					.show();
		}
	}
	
	@Override
	public void onStop() {
		super.onStop();
		
		SharedPreferences.Editor editor = sharedPref.edit();
		if (isPlaying) {			
			editor.putInt(SELECTED_RADIO, selectedRadio);			
		} else {
			editor.putInt(SELECTED_RADIO, 0);
		}
		editor.commit();
	}

	public class MediaLoadedReceiver extends ResultReceiver {

		public MediaLoadedReceiver(Handler handler) {
			super(handler);
		}

		@Override
		protected void onReceiveResult(int resultCode, Bundle resultData) {
			super.onReceiveResult(resultCode, resultData);
			handlerOnUI.post(updateButtonEnabledThread);
		}
	}

	public void initRunnable() {

		runInUI = new Runnable() {
			@Override
			public void run() {
				switch (err) {
				case ERR_CONNEXION:
					radioSubtitle.setText("");
					break;
				case FAILED:
					radioSubtitle.setText("");
					break;
				case OK:
					imgLive.setImageBitmap(bitmap);
					shadow.setVisibility(View.VISIBLE);
					radioTitle.setText(radios.get(selectedRadio).getName());
					radioSubtitle.setText(currentSongMetadata.getTitle()
							+ " - " + currentSongMetadata.getArtist());
					break;
				case WARNING:
					break;
				}

			}
		};

		getPictureUrl = new Runnable() {
			@Override
			public void run() {
				try {
					String radioId = radios.get(selectedRadio).getRadioId();
					SongMetadata metadata = JSONparser.GetCurrentTrack(radioId);
					if (!metadata.getMediaCover().equals(lastPicture)) {
						lastPicture = metadata.getMediaCover();
						radios.get(selectedRadio).setCurrentSongMetadata(
								metadata);
						currentSongMetadata = metadata;
						bitmap = ImageFetcher.GetImageFromUrl(metadata
								.getMediaCover());
						err = ResponseCode.OK;
						handlerToSetPicture.post(runInUI);
					}
				} catch (ClientProtocolException e) {
					err = ResponseCode.ERR_CONNEXION;
					e.printStackTrace();
				} catch (JSONException e) {
					err = ResponseCode.FAILED;
					e.printStackTrace();
				} catch (IOException e) {
					err = ResponseCode.FAILED;
					e.printStackTrace();
				}
			}
		};
	}
	
	
	@Override
	public void onResume() {
        super.onResume();
       // String activityNameOnCapptain = CapptainAgentUtils.buildCapptainActivityName(getClass()); // Uses short class name and removes "Activity" at the end.
       // Log.d("activityNameOnCapptain","==="+activityNameOnCapptain);
       // CapptainAgent.getInstance(getActivity()).startActivity(getActivity(), activityNameOnCapptain, null);
        uiHelper.onResume();
    }

    @Override
	public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        uiHelper.onSaveInstanceState(outState);

        outState.putString(PENDING_ACTION_BUNDLE_KEY, pendingAction.name());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        uiHelper.onActivityResult(requestCode, resultCode, data, dialogCallback);
    }

    @Override
    public void onPause() {
        super.onPause();
       // CapptainAgent.getInstance(getActivity()).endActivity();
        uiHelper.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        uiHelper.onDestroy();
    }

    private void onSessionStateChange(Session session, SessionState state, Exception exception) {
        if (pendingAction != PendingAction.NONE &&
                (exception instanceof FacebookOperationCanceledException ||
                exception instanceof FacebookAuthorizationException)) {
                new AlertDialog.Builder(activity)
                    .setTitle(R.string.cancelled)
                    .setMessage(R.string.permission_not_granted)
                    .setPositiveButton(R.string.ok, null)
                    .show();
            pendingAction = PendingAction.NONE;
        } else if (state == SessionState.OPENED_TOKEN_UPDATED) {
            handlePendingAction();
        }
    }
    
    @SuppressWarnings("incomplete-switch")
    private void handlePendingAction() {
        PendingAction previouslyPendingAction = pendingAction;
        // These actions may re-set pendingAction if they are still pending, but we assume they
        // will succeed.
        pendingAction = PendingAction.NONE;

        switch (previouslyPendingAction) {
            case POST_PHOTO:
                postPhoto();
                break;
        }
    }
    
    private void onClickPostPhoto() {
        performPublish(PendingAction.POST_PHOTO, false);
    }
    
    private void postPhoto() {
        if (hasPublishPermission()) {
            publishFeedDialog();
        } else {
            pendingAction = PendingAction.POST_PHOTO;
        }
    }
    
    private void publishFeedDialog() {
    	String description;
		if (isPlaying && currentSongMetadata.getArtist() != null){
			description = radios.get(selectedRadio).getShare().getTweet(currentSongMetadata.getArtist(),currentSongMetadata.getTitle());
		} else {
			description = "now playing sur @RFMFrance";
		}
		
		String urlLogo;
		switch (selectedRadio){
		case 2:urlLogo = getString(R.string.logo_rfm_night);
				break;
		default:urlLogo = getString(R.string.logo_rfm_live);
				break;
		case 1:urlLogo = getString(R.string.logo_rfm_collector);
				break;
		}
		
        Bundle params = new Bundle();
        params.putString("name", radios.get(selectedRadio).getName());
        params.putString("description", description);
        params.putString("link", "http://www.rfm.fr");
        params.putString("picture", urlLogo);

        WebDialog feedDialog = (
            new WebDialog.FeedDialogBuilder(activity,
                Session.getActiveSession(),
                params))
            .setOnCompleteListener(new OnCompleteListener() {
                @Override
                public void onComplete(Bundle values, FacebookException error) {
                    if (error == null) {
                        // When the story is posted, echo the success
                        // and the post Id.
                        final String postId = values.getString("post_id");
                        if (postId != null) {
                            Toast.makeText(activity, getString(R.string.facebook_post_published), Toast.LENGTH_SHORT).show();
                        } else {
                            // User clicked the Cancel button
                            Toast.makeText(activity.getApplicationContext(), getString(R.string.facebook_post_cancelled), Toast.LENGTH_SHORT).show();
                        }
                    } else if (error instanceof FacebookOperationCanceledException) {
                        // User clicked the "x" button
                        Toast.makeText(activity.getApplicationContext(), getString(R.string.facebook_post_cancelled), Toast.LENGTH_SHORT).show();
                    } else {
                        // Generic, ex: network error
                        Toast.makeText(activity.getApplicationContext(), getString(R.string.facebook_post_publishing_failed), Toast.LENGTH_SHORT).show();
                    }
                }

            })
            .build();
        feedDialog.show();
    }

    private boolean hasPublishPermission() {
        Session session = Session.getActiveSession();
        return session != null && session.getPermissions().contains("publish_actions");
    }

    private void performPublish(PendingAction action, boolean allowNoSession) {
        Session session = Session.getActiveSession();
        if (session != null) {
            pendingAction = action;
            if (hasPublishPermission()) {
                // We can do the action right away.
                handlePendingAction();
                return;
            } else if (session.isOpened()) {
                // We need to get new permissions, then complete the action when we get called back.
                session.requestNewPublishPermissions(new Session.NewPermissionsRequest(activity, PERMISSION));
                return;
            }
        }

        if (allowNoSession) {
            pendingAction = action;
            handlePendingAction();
        }
    }
}
