package com.rfm.android.fragment;

import java.util.Arrays;
import java.util.List;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.widget.FacebookDialog;
import com.rfm.android.ApplicationData;
import com.rfm.android.R;

public class AccountFragment extends Fragment {
	private View view;
	private ImageView website, facebookImg, twitter; 
	
	Activity mActivity;
	
	private Session.StatusCallback statusCallback = new SessionStatusCallback();
	private UiLifecycleHelper uiHelper;
	private Fragment fragmentContext;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		fragmentContext = this;
		
		uiHelper = new UiLifecycleHelper(getActivity(), statusCallback);
		uiHelper.onCreate(savedInstanceState);

		mActivity = getActivity();
		super.onCreateView(inflater, container, savedInstanceState);		
		view = inflater.inflate(R.layout.fragment_account, container, false);
		
		website  = (ImageView) view.findViewById(R.id.btn_access_website);
		facebookImg = (ImageView) view.findViewById(R.id.btn_share_fb);
		twitter  = (ImageView) view.findViewById(R.id.btn_share_twitter);
		
		TextView textCommunity = (TextView) view.findViewById(R.id.tv_community);
		
		if (textCommunity != null) {
			textCommunity.setTypeface(ApplicationData.getTypeFaceRegular(getActivity()
					.getAssets()));
		}
		
		setListeners();
		return view;
	}
	
	public void setListeners () {
		OnClickListener onShareListener = new OnClickListener() {			
			@Override
			public void onClick(View v) {
				switch (v.getId()) {
				case R.id.btn_access_website :
					Intent browserIntent = new Intent(Intent.ACTION_VIEW);
					browserIntent.setData(Uri.parse(getString(R.string.url_radio)));
					startActivity(browserIntent);
					break;
				case R.id.btn_share_fb :
					ShareOnFacebook();
					break;
				case R.id.btn_share_twitter :
					ShareOnTwitter();
					break;
				}				
			}
		};
		
		website.setOnClickListener(onShareListener);
		facebookImg.setOnClickListener(onShareListener);
		twitter.setOnClickListener(onShareListener);
	}
	
	public void ShareOnFacebook (){

		Session.openActiveSession(getActivity(), this, true, statusCallback);
	}


	public void ShareOnTwitter (){
		Intent tweetIntent = new Intent(Intent.ACTION_SEND);
		tweetIntent.setType("text/plain");
		if (setTwitterPackage(tweetIntent)) {
			tweetIntent.putExtra(Intent.EXTRA_TEXT, "http://www.rfm.fr/mobile");
			startActivity(tweetIntent);
		} else {
			try {
				tweetIntent.setAction(Intent.ACTION_VIEW);
				tweetIntent.setData(Uri.parse("market://details?id="
						+ "com.twitter.android"));
				startActivity(tweetIntent);
			} catch (ActivityNotFoundException e) {
				// in case device has not Google Play anymore ...
				Toast.makeText(mActivity,
						mActivity.getText(R.string.install_twitter),
						Toast.LENGTH_SHORT).show();
			}
		}
	}
	
	protected boolean setTwitterPackage(Intent tweetIntent) {
		// best-known twitter package names
		final String[] twitterPackage = { "com.twitter.android",
				"com.twidroid", "com.handmark.tweetcaster",
				"com.thedeck.android" };
		final PackageManager packageManager = mActivity.getPackageManager();
		List<ResolveInfo> list = packageManager.queryIntentActivities(
				tweetIntent, PackageManager.MATCH_DEFAULT_ONLY);
		for (ResolveInfo resolveInfo : list) {
			String p = resolveInfo.activityInfo.packageName;
			for (int i = 0; i < twitterPackage.length; i++) {
				if (p != null && p.startsWith(twitterPackage[i])) {
					tweetIntent.setPackage(p);
					return true;
				}
			}
		}
		return false;
	}
	
	private class SessionStatusCallback implements Session.StatusCallback {

		@Override
		public void call(Session session, SessionState state,
				Exception exception) {
			if (session.isOpened()){

				Session.NewPermissionsRequest newPermissionsRequest = new Session
						.NewPermissionsRequest(fragmentContext, Arrays.asList("publish_actions"));
				session.requestNewPublishPermissions(newPermissionsRequest);

				openShareDialog();
			}	

			if (state == SessionState.CLOSED_LOGIN_FAILED){
				Toast.makeText(getActivity(),getString(R.string.facebook_cannot_login), Toast.LENGTH_SHORT).show();
			}
		}
	}
	
	private void openShareDialog () {
	
		
        FacebookDialog.ShareDialogBuilder builder = new FacebookDialog.ShareDialogBuilder(getActivity())
		.setLink("http://www.rfm.fr")
		.setPicture(getString(R.string.logo_rfm_live))
		.setName("RFM")
		.setFragment(this);

		// share the app
		if (builder.canPresent()) {
		    builder.build().present();
		}
	}
}
