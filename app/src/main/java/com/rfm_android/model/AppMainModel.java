package com.rfm.android.model;

import java.util.ArrayList;

public class AppMainModel {

	private String version;
	private ArrayList<Radio> radios;
	private String podcast_url;
	private Share podcast_share;
	private String program_url;
	private String events_url;

	public AppMainModel() {
		super();
		this.version = "";

		// à commenter tant que le flux n'est pas dynamique -> setRadio()
		//this.radios = radios;
		this.radios = new ArrayList<Radio>();
		this.radios.add(new Radio("9612321", "RFM", "http://mp3lg3.tdf-cdn.com/9141/lag_101428.mp3", "", new Share("http://rfm.fr", "J'\u00E9coute %artist% - %track% sur RFM", "RFM", "")));
		this.radios.add(new Radio("9612321", "RFM COLLECTOR", "http://mp3lg3.tdf-cdn.com/9141/lag_101428.mp3", "", new Share("http://rfm.fr","J'\u00E9coute %artist% - %track% sur RFM Collector", "RFM Collector", "")));
		this.radios.add(new Radio("9612577", "RFM NIGHT FEVER", "http://mp3lg3.tdf-cdn.com/9142/lag_102409.mp3", "", new Share("http://rfm.fr", "J'\u00E9coute %artist% - %track% sur RFM Night Fever", "RFM Night Fever", "")));
		
		this.podcast_url = "";
		this.podcast_share = new Share();
		this.program_url = "";
		this.events_url = "";
	}

	public AppMainModel(String version, ArrayList<Radio> radios,
			String podcast_url, Share podcast_share, String program_url,
			String events_url) {
		super();
		this.version = version;
		
		// à commenter tant que le flux n'est pas dynamique
		//this.radios = radios;

		this.radios = new ArrayList<Radio>();
		this.radios.add(new Radio("9612321", "RFM", "http://mp3lg3.tdf-cdn.com/9141/lag_101428.mp3", "", new Share("http://rfm.fr","J'\u00E9coute %artist% - %track% sur RFM", "RFM", "")));
		this.radios.add(new Radio("9612321", "RFM COLLECTOR", "http://mp3lg3.tdf-cdn.com/9141/lag_101428.mp3", "", new Share("http://rfm.fr","J'\u00E9coute %artist% - %track% sur RFM Collector", "RFM Collector", "")));
		this.radios.add(new Radio("9612577", "RFM NIGHT FEVER", "http://mp3lg3.tdf-cdn.com/9142/lag_102409.mp3", "", new Share("http://rfm.fr","J'\u00E9coute %artist% - %track% sur RFM Night Fever", "RFM Night Fever", "")));

		this.podcast_url = podcast_url;
		this.podcast_share = podcast_share;
		this.program_url = program_url;
		this.events_url = events_url;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public ArrayList<Radio> getRadios() {
		return radios;
	}

	public void setRadios(ArrayList<Radio> radios) {
		//this.radios = radios;
	}

	public String getPodcast_url() {
		return podcast_url;
	}

	public void setPodcast_url(String podcast_url) {
		this.podcast_url = podcast_url;
	}

	public Share getPodcast_share() {
		return podcast_share;
	}

	public void setPodcast_share(Share podcast_share) {
		this.podcast_share = podcast_share;
	}

	public String getProgram_url() {
		return program_url;
	}

	public void setProgram_url(String program_url) {
		this.program_url = program_url;
	}

	public String getEvents_url() {
		return events_url;
	}

	public void setEvents_url(String events_url) {
		this.events_url = events_url;
	}
	
	
}
