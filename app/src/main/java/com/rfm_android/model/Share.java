package com.rfm.android.model;

public class Share {

	private String facebook_url;
	private String tweet;
	private String mail_subject;
	private String mail_body;

	public Share() {
		super();
		this.facebook_url = "";
		this.tweet = "";
		this.mail_subject = "";
		this.mail_body = "";
	}

	public Share(String facebook_url, String tweet, String mail_subject, String mail_body) {
		this.facebook_url = facebook_url;
		this.tweet = tweet;
		this.mail_subject = mail_subject;
		this.mail_body = mail_body;
	}

	public String getFacebook_url() {
		return facebook_url;
	}

	public void setFacebook_url(String facebook_url) {
		this.facebook_url = facebook_url;
	}

	public String getTweet(String artist, String track) {
		return tweet.replace("%artist%", artist).replace("%track%", track);
	}
	
	public String getTweetPodcast(String category, String title) {
		return tweet.replace("%podcast_category_name%", category).replace("%podcast_name%", title);
	}


	public void setTweet(String tweet) {
		this.tweet = tweet;
	}

	public String getMail_subject() {
		return mail_subject;
	}

	public void setMail_subject(String mail_subject) {
		this.mail_subject = mail_subject;
	}

	public String getMail_body() {
		return mail_body;
	}

	public void setMail_body(String mail_body) {
		this.mail_body = mail_body;
	}

}
