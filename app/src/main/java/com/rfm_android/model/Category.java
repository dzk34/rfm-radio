package com.rfm.android.model;

import java.util.ArrayList;
import java.util.Collections;

import android.os.Parcel;
import android.os.Parcelable;

public class Category implements Parcelable {
	String title;
	String img;
	String descr = "";
	ArrayList<Podcast> podcasts;
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getImg() {
		return img;
	}
	public void setImg(String img) {
		this.img = img;
	}
	public String getDescr() {
		return descr;
	}
	public void setDescr(String descr) {
		this.descr = descr;
	}
	public ArrayList<Podcast> getPodcasts() {
		return podcasts;
	}
	public void setPodcasts(ArrayList<Podcast> podcasts) {
		this.podcasts = podcasts;
	}
	
	public Category(String title, ArrayList<Podcast> podcasts, String imgUrl, String description) {
		this.title = title;
		this.podcasts = podcasts;
		//this.descr = description;
		this.img = imgUrl;
		
		Collections.sort(this.podcasts, Collections.reverseOrder());
	}
	
	public Category(Parcel p){
		title = p.readString();
		descr = p.readString();
		img = p.readString();
		podcasts = new ArrayList<Podcast>();
		p.readList(podcasts, Podcast.class.getClassLoader());
	}
	
	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		dest.writeString(title);
		dest.writeString(descr);
		dest.writeString(img);
		dest.writeList(podcasts);
	}
	
	public static final Parcelable.Creator<Category> CREATOR = new Parcelable.Creator<Category>() {
		public Category createFromParcel(Parcel in) {
			return new Category(in);
		}

		public Category[] newArray(int size) {
			return new Category[size];
		}
	};
}
