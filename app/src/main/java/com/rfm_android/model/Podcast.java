package com.rfm.android.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.os.Parcel;
import android.os.Parcelable;

public class Podcast implements Parcelable, Comparable<Podcast> {
	String title;
	String date;
	String filepath;
	String thumb;
	String description;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		//format date			
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
		SimpleDateFormat sdfFinal = new SimpleDateFormat("dd/MM/yyyy",
				Locale.FRENCH);
		Date getDate = null;
		try {
			getDate = sdf.parse(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String finalDate = sdfFinal.format(getDate);
		this.date = finalDate;
	}

	public String getFilepath() {
		return filepath;
	}

	public void setFilepath(String filepath) {
		this.filepath = filepath;
	}

	public String getThumb() {
		return thumb;
	}

	public void setThumb(String thumb) {
		this.thumb = thumb;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Podcast(String title, String date, String filepath, String thumb,
			String description) {
		this.title = title;
		setDate(date);
		
		if(filepath != null)
			this.filepath = filepath.replace(" ", "%20");
		
		this.thumb = thumb;
		this.description = description;
	}

	public Podcast(Parcel parc) {
		this.title = parc.readString();
		this.date = parc.readString();
		this.filepath = parc.readString();
		this.thumb = parc.readString();
		this.description = parc.readString();
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		dest.writeString(title);
		dest.writeString(date);
		dest.writeString(filepath);
		dest.writeString(thumb);
		dest.writeString(description);
	}

	public static final Parcelable.Creator<Podcast> CREATOR = new Parcelable.Creator<Podcast>() {
		public Podcast createFromParcel(Parcel in) {
			return new Podcast(in);
		}

		public Podcast[] newArray(int size) {
			return new Podcast[size];
		}
	};

	@Override
	public int compareTo(Podcast another) {
		//create date objects
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy",
				Locale.FRENCH);
		Date mDate = null;
		Date aDate = null;
		try {
			mDate = sdf.parse(date);
			aDate = sdf.parse(another.getDate());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return mDate.compareTo(aDate);
	}

}
