package com.rfm.android.model;

import android.os.Parcel;
import android.os.Parcelable;

public class SongMetadata implements Parcelable {

	private String title;
	private String artist;
	private String mediaCover;
	
	public SongMetadata(){
		
	}
	
	public SongMetadata(Parcel in){
		this.title = in.readString();
		this.artist = in.readString();
		this.mediaCover = in.readString();
	}
	
	public SongMetadata(String title, String artist, String mediaCover) {
		super();
		this.title = title;
		this.artist = artist;
		this.mediaCover = mediaCover;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getArtist() {
		return artist;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}

	public String getMediaCover() {
		return mediaCover;
	}

	public void setMediaCover(String mediaCover) {
		this.mediaCover = mediaCover;
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flag) {
		dest.writeString(this.title);
		dest.writeString(this.artist);
		dest.writeString(this.mediaCover);
	}

	public static final Parcelable.Creator<SongMetadata> CREATOR = new Parcelable.Creator<SongMetadata>() {
		public SongMetadata createFromParcel(Parcel in) {
			return new SongMetadata(in);
		}

		public SongMetadata[] newArray(int size) {
			return new SongMetadata[size];
		}
	};
}
