package com.rfm.android.model;

public class Radio {

	private String radioId;
	private String name;
	private String live_url;
	private String logo;
	private String freq;
	private String city;
	private SongMetadata currentSongMetadata;
	
	private Share share;
	
	public Radio(){
		
	}
	
	public Radio(String _radioId, String _name, String _live_url, String _logo, Share _share){
		radioId = _radioId;
		name = _name;
		live_url = _live_url;
		logo = _logo;
		share = _share;
		currentSongMetadata = new SongMetadata();
	}
	
	
	public String getFreq() {
		return freq;
	}

	public void setFreq(String freq) {
		this.freq = freq;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getRadioId() {
		return radioId;
	}

	public void setRadioId(String radioId) {
		this.radioId = radioId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLive_url() {
		return live_url;
	}
	public void setLive_url(String live_url) {
		this.live_url = live_url;
	}
	public String getLogo() {
		return logo;
	}
	public void setLogo(String logo) {
		this.logo = logo;
	}

	public Share getShare() {
		return share;
	}

	public void setShare(Share share) {
		this.share = share;
	}

	public SongMetadata getCurrentSongMetadata() {
		return currentSongMetadata;
	}

	public void setCurrentSongMetadata(SongMetadata currentSongMetadata) {
		this.currentSongMetadata = currentSongMetadata;
	}
	
}
