package com.rfm.android.mediaplayer;

import com.spoledge.aacdecoder.AACPlayer;
import com.spoledge.aacdecoder.PlayerCallback;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.IBinder;
import android.os.ResultReceiver;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.MediaController.MediaPlayerControl;

public class BackgroundMediaPlayer extends Service implements
		MediaPlayer.OnPreparedListener, MediaPlayer.OnErrorListener, MediaPlayer.OnBufferingUpdateListener {

	private final IBinder mBinder = new MyBinder();
	private ResultReceiver mediaReceiver;

	private PhoneStateListener phoneListener;

	public class MyBinder extends Binder {
		public BackgroundMediaPlayer getBackgroundPlayer() {
			return BackgroundMediaPlayer.this;
		}
	}

	private static AACPlayer mAACplayer;
	private String url;
	private NOW_PLAYING now_playing;

	private MediaPlayer MP4Player;
	private boolean isPlayerPreparing = false;
	private int buffering = 0;
	
	private boolean isAACPlaying = false;
	
	private boolean manuallyPaused = true;

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();

		phoneListener = new PhoneStateListener() {
			@Override
			public void onCallStateChanged(int state, String incomingNumber) {
				// TODO Auto-generated method stub
				super.onCallStateChanged(state, incomingNumber);

				switch (state) {
				case TelephonyManager.CALL_STATE_RINGING:
					if(now_playing == NOW_PLAYING.AAC){
						if(isAACPlaying){
							pauseAACPlayer(false);
						}
						else{
							pauseAACPlayer(true);
						}
					}
					else{
						pauseMP4Player(false);
					}
					break;
				case TelephonyManager.CALL_STATE_IDLE:
					if(now_playing == NOW_PLAYING.AAC && !manuallyPaused){
						startAACPlayer(url);
					}
					else if(!manuallyPaused){
						playMP4Player();
					}
					break;
				}
			}
		};
		
		TelephonyManager tm = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
		if(tm != null){
			tm.listen(phoneListener, PhoneStateListener.LISTEN_CALL_STATE);
		}
	}
	
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		TelephonyManager tm = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
		if(tm != null){
			tm.listen(phoneListener, PhoneStateListener.LISTEN_NONE);
		}
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {

		try {
			if (intent.getAction().equals(MediaAction.PLAY_AAC.toString())) {
				mediaReceiver = intent.getExtras().getParcelable("receiver");
				url = intent.getExtras().getString("url");
				now_playing = NOW_PLAYING.AAC;
				startAACPlayer(url);
			} else if (intent.getAction().equals(
					MediaAction.PAUSE_ALL.toString())) {
				stopPlayers(true);
			} else if (intent.getAction().equals(
					MediaAction.PLAY_MP4.toString())) {
				url = intent.getExtras().getString("url");
				now_playing = NOW_PLAYING.MP4;
				stopPlayers(true);

				MP4Player = new MediaPlayer();
				MP4Player.setDataSource(url);
				MP4Player.setOnPreparedListener(this);
				MP4Player.setOnBufferingUpdateListener(this);
				isPlayerPreparing = true;
				MP4Player.prepareAsync();
				//MP4Player.start();

			} else if (intent.getAction().equals(
					MediaAction.PAUSE_MP4.toString())) {
				pauseMP4Player(true);
			} else if (intent.getAction().equals(
					MediaAction.RESUME_MP4.toString())) {
				playMP4Player();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return 1;
	}

	private void startAACPlayer(String url) {
		if( ! isAACPlaying){
			stopPlayers(true);
			mAACplayer = new AACPlayer();
			mAACplayer.setPlayerCallback(new PlayerCallback() {
	
				@Override
				public void playerStopped(int arg0) {
					// TODO Auto-generated method stub
				}
	
				@Override
				public void playerStarted() {
					if (mediaReceiver != null)
						mediaReceiver.send(200, null);
				}
	
				@Override
				public void playerPCMFeedBuffer(boolean arg0, int arg1, int arg2) {
				}
	
				@Override
				public void playerMetadata(String arg0, String arg1) {
	
				}
	
				@Override
				public void playerException(Throwable arg0) {
	
				}
			});
			mAACplayer.playAsync(url);
			isAACPlaying = true;
			now_playing = NOW_PLAYING.AAC;
			manuallyPaused = false;
			Log.d("AACPlayer", "playing !");
		}
	}

	public void stopPlayers(boolean _manuallyPaused) {
		if (mAACplayer != null) {
			mAACplayer.stop();
			Log.d("AACPLayer", "stoped !");
		}
		isAACPlaying = false;
		stopMP4Player(_manuallyPaused);
		manuallyPaused = _manuallyPaused;
	}
	
	private void pauseAACPlayer(boolean _manuallyPaused){
		if (mAACplayer != null) {
			mAACplayer.stop();
			manuallyPaused = _manuallyPaused;
			isAACPlaying = false;
			if(manuallyPaused)
			{
				now_playing = NOW_PLAYING.NOTHING;
			}
			Log.d("AACPLayer", "stoped !");
		}
	}

	public void stopMP4Player(boolean _manuallyPaused) {
		if (MP4Player != null) {
			MP4Player.stop();
			MP4Player.reset();
			MP4Player = null;
			manuallyPaused = _manuallyPaused;
		}
	}

	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return mBinder;
	}

	@Override
	public boolean onError(MediaPlayer mp, int what, int extra) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void onPrepared(MediaPlayer mp) {
		isPlayerPreparing = false;
		playMP4Player();
	}
	
	public boolean isPreparing(){
		return isPlayerPreparing;
	}

	public void pauseMP4Player(boolean _manuallyPaused) {
		if (MP4Player != null) {
			MP4Player.pause();
			manuallyPaused = _manuallyPaused;
		}
	}

	public void playMP4Player() {
		if (MP4Player != null) {
			MP4Player.start();
			manuallyPaused = false;
		}
	}

	public int getCurrentPosition() {
		// TODO Auto-generated method stub
	
		
		if (MP4Player != null) {
			return MP4Player.getCurrentPosition();
		} else {
			return 0;
		}
	}

	public int getDuration() {
		if(isPlayerPreparing){
			return 0;
		} else if (MP4Player != null) {
			return MP4Player.getDuration();
		} else {
			return 0;
		}
	}

	public boolean isPlaying() {
		// TODO Auto-generated method stub
		if (MP4Player != null) {
			return MP4Player.isPlaying();
		} else {
			return false;
		}
	}

	public void seekTo(int pos) {
		// TODO Auto-generated method stub
		if (MP4Player != null) {
			MP4Player.seekTo(pos);
		}
	}
	
	public int getBufferedPercent(){
		return buffering;
	}
	
	private enum NOW_PLAYING{
		AAC,
		MP4,
		NOTHING
	}

	@Override
	public void onBufferingUpdate(MediaPlayer mp, int percent) {
		buffering = percent;
	}
}
