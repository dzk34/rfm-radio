package com.rfm.android.mediaplayer;

public enum MediaAction {

	PLAY_AAC("PLAY_AAC"),
	PLAY_MP4("PLAY_MP4"),
	PAUSE_ALL("PAUSE_ALL"),
	PAUSE_MP4("PAUSE_MP4"),
	RESUME_MP4("RESUME");
	
	private String action;
	
	MediaAction(String m_action){
		action = m_action;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return action;
	}
}
