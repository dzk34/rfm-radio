package com.rfm.android;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.ldf.lamosel.version.VersionApplication;
import com.ldf.lamosel.voting.VotingApplication;
import com.rfm.android.activity.BaseActivity;
import com.rfm.android.activity.MainActivity;
import com.rfm.android.all.ResponseCode;
import com.rfm.android.fragment.LiveFragment;
import com.rfm.android.model.Category;
import com.rfm.android.model.Podcast;
import com.rfm.android.model.Radio;
import com.rfm.android.util.ImageFetcher;
import com.rfm.android.util.InternetConnection;
import com.rfm.android.util.JSONparser;

public class Splash extends BaseActivity implements
		GooglePlayServicesClient.ConnectionCallbacks, LocationListener,
		OnConnectionFailedListener {
	public static final int UPDATE_INTERVAL_IN_SECONDS = 1;
	private static final long UPDATE_INTERVAL = 1000 * UPDATE_INTERVAL_IN_SECONDS;
	private static final int FASTEST_INTERVAL_IN_SECONDS = 1;
	private static final int TIMER_COUNT = 8000;

	LocationRequest mLocationRequest;
	LocationClient mLocationClient;
	Location mCurrentLocation = null;

	Context mContext;
	public ProgressBar progressBar1;
	long m_dwSplashTime = 2500;
	boolean m_bPaused = false;
	boolean m_bSplashActive = true;
	protected int _splashTime = 2000;
	private Thread splashTread;
	ArrayList<Category> arraylist = new ArrayList<Category>();
	private ResponseCode err = ResponseCode.OK;
	boolean TIME_OUT = false;

	private static final String RFM_FILENAME = "RFM.txt";
	private static final String PODCAST_FILENAME = "PODCAST.txt";

	String responce = "";

	Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			progressBar1.setVisibility(View.INVISIBLE);
			switch (err) {
			case ERR_CONNEXION:
				Toast.makeText(getApplicationContext(),
						"Une connexion à internet est requise",
						Toast.LENGTH_SHORT).show();
				break;
			case FAILED:
				Toast.makeText(getApplicationContext(),
						"Une erreur s'est produite", Toast.LENGTH_SHORT).show();
				break;
			}
		}
	};

	@Override
	public void onCreate(Bundle savedInstanceState) {
		VersionApplication.getInstance().checkVersion(this, this);
		VotingApplication.getInstance().startChrono(this);
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.splash);
		
		ImageFetcher.InitCache();
		mContext = this;

		try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.rfm.android", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.e("MY KEY HASH:",
                        Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {
        	
        }
		
		progressBar1 = (ProgressBar) findViewById(R.id.progressBar1);

		// thread for displaying the SplashScreen
		splashTread = new Thread() {
			String latitude;
			String longitude;

			
			@Override
			public void run() {
				try {
						long l1 = System.currentTimeMillis();
						long l2 = l1;
						//waiting for location to be set or time_out
						while (mCurrentLocation == null && (l2-l1) < TIMER_COUNT){
							l2 = System.currentTimeMillis();
						}
						TIME_OUT  = true;
						if (mCurrentLocation == null){
							latitude = null;
							longitude = null;
						} else {
							latitude = Double.toString(mCurrentLocation
									.getLatitude());
							longitude = Double.toString(mCurrentLocation
									.getLongitude());
							ApplicationData.IS_LOCATION_SET = true;
						}
			

					if (InternetConnection.isConnected(mContext)) {
						
						if (latitude != null && longitude != null) {
							try {			
								
								JSONObject rootObject = JSONparser.GetJSONGetMethod(getString(R.string.main_url));
								ApplicationData.setMainModel(JSONparser
										.parseMainJSON(rootObject));
								
								JSONObject liveFeedObj = JSONparser
										.GetJSONAuthenticatedRequest(
												"http://www.rfm.fr/ws/geolocalisation/flux?lat=",
												latitude, longitude);

								Radio radio = JSONparser
										.parseLiveFeed(liveFeedObj);
								writeJsonInFile(RFM_FILENAME,
										liveFeedObj.toString());

								Radio liveRadio = ApplicationData.getMainModel().getRadios().get(0);
								liveRadio.setCity(radio.getCity());
								liveRadio.setFreq(radio.getFreq());
								liveRadio.setName(radio.getName());
								liveRadio.setLive_url(radio.getLive_url());
								liveRadio.setRadioId(radio.getRadioId());
										
								if (ApplicationData.getMainModel().getRadios().size() == 2){
									ApplicationData.getMainModel().getRadios().add(0, liveRadio);
								} else {
									ApplicationData.getMainModel().getRadios().set(0, liveRadio);
								}

							} catch (Exception e) {

								ApplicationData.getMainModel();
								JSONObject rootObject = JSONparser
										.GetJSONGetMethod(getString(R.string.main_url));

								writeJsonInFile(RFM_FILENAME,
										rootObject.toString());
								ApplicationData.setMainModel(JSONparser
										.parseMainJSON(rootObject));
							}
						} else {
							JSONObject rootObject = JSONparser
									.GetJSONGetMethod(getString(R.string.main_url));

							writeJsonInFile(RFM_FILENAME, rootObject.toString());
							ApplicationData.setMainModel(JSONparser
									.parseMainJSON(rootObject));
						}

						// à décommenter quand le l'url des podcasts est fixée
						responce = JSONparser.GetJSONGetMethod(ApplicationData.getMainModel().getPodcast_url()).toString();
						writeJsonInFile(PODCAST_FILENAME, responce);

						ApplicationData.IS_CACHE_DATA_AVAILABLE = true;
					} else {
						err = ResponseCode.ERR_CONNEXION;
						ApplicationData.IS_CONNECTED_TO_INTERNET = false;

						retrieveCacheData();
					}
					handler.sendMessage(handler.obtainMessage());

					parseJSON();

				} catch (Exception e) {
					e.printStackTrace();
					err = ResponseCode.FAILED;

					retrieveCacheData();
				} finally {
					if (ApplicationData.getMainModel().getRadios().size() == 2){
						ApplicationData.getMainModel().getRadios().add(0, null);
					}
					Intent i = new Intent(Splash.this, MainActivity.class);
					startActivityForResult(i, 0);
					//finish();
				}

			}
		};
		splashTread.start();

	}

	private void retrieveCacheData() {
		try {
			ApplicationData
					.setMainModel(JSONparser.parseMainJSON(new JSONObject(
							readJsonInFile(RFM_FILENAME))));
			responce = readJsonInFile(PODCAST_FILENAME);
			ApplicationData.IS_CACHE_DATA_AVAILABLE = true;
		} catch (Exception e) {
			Log.d("retriveCacheData", e.getMessage()+"");
			e.printStackTrace();
		}

	}

	private void writeJsonInFile(String filename, String object) {

		try {
			FileOutputStream fos = openFileOutput(filename,
					Context.MODE_PRIVATE);
			fos.write(object.toString().getBytes());
			fos.close();

			Log.d("JSON writing", "OK");
		} catch (IOException e) {
			e.printStackTrace();
			Log.d("JSON writing", "ERROR " + e.getMessage());
		}
	}

	private String readJsonInFile(String filename) {

		try {
			FileInputStream fis = openFileInput(filename);
			StringBuffer strBuffer = new StringBuffer();
			int ch;
			while ((ch = fis.read()) != -1) {
				strBuffer.append((char) ch);
			}
			fis.close();
			Log.d("JSON reading", "OK");

			return strBuffer.toString();

		} catch (Exception e) {
			e.printStackTrace();
			Log.d("JSON reading", "ERROR " + e.getMessage());

			return null;
		}
	}

	public void parseJSON() throws JSONException {

		if (!responce.equals("")) {

			ArrayList<Podcast> arraylist_podcast = new ArrayList<Podcast>();
			JSONObject jsonFeed = new JSONObject(responce);
			if (jsonFeed.length() != 0) {

				JSONArray categories = jsonFeed.getJSONArray("categories");
				JSONObject jsonObj = null;
				for (int i = 0; i < categories.length(); i++) {
					arraylist_podcast = new ArrayList<Podcast>();
					jsonObj = categories.getJSONObject(i);

					JSONArray podcasts = jsonObj.getJSONArray("podcasts");
					JSONObject jsonObj1 = null;
					for (int j = 0; j < podcasts.length(); j++) {
						jsonObj1 = podcasts.getJSONObject(j);
						String url = jsonObj1.getString("cover_url");
						if (!url.contains(".jpg")) {
							url = url + ".jpg";
						}

						arraylist_podcast.add(new Podcast(jsonObj1
								.getString("title"),
								jsonObj1.getString("date"), jsonObj1
										.getString("filepath"), url, jsonObj1
										.getString("description")));
					}
					// replace null by the category's description
					arraylist.add(new Category(jsonObj.getString("title"),
							arraylist_podcast, arraylist_podcast.get(0)
									.getThumb(), null));
				}

				ApplicationData.setCategories(arraylist);
			}
		}
	}

	@Override
	protected void onStart() {
		super.onStart();
		// check whether we are connected to GooglePlayServices
		// before setting location functionality

		if (servicesConnected()) {
			mLocationClient = new LocationClient(this, this, this);
			mLocationRequest = LocationRequest.create();

			// set update parameters
			mLocationRequest
					.setPriority(LocationRequest.PRIORITY_NO_POWER);
			mLocationRequest.setInterval(UPDATE_INTERVAL);
			// mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
			mLocationClient.connect();
		}
		
	}

	@Override
	protected void onDestroy() {

		try {
			if (mLocationClient.isConnected()) {
				mLocationClient.removeLocationUpdates(this);
				mLocationClient.disconnect();
			}	
		} catch (Exception e) {
			e.printStackTrace();
		}
		super.onStop();

	}

	// find user's location
	private boolean servicesConnected() {

		int resultCode = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(this);

		if (ConnectionResult.SUCCESS == resultCode) {
			Log.d("Location Updates", "Google Play services is available.");
			return true;

		} else {
			Log.d("Location Updates", "Google Play services is not available.");
		}
		return false;
	}

	@Override
	public void onLocationChanged(Location location) {
		mCurrentLocation = location;
		
		if (TIME_OUT && location!= null && ApplicationData.IS_CONNECTED_TO_INTERNET){
			new Thread(new Runnable() {
				
				@Override
				public void run() {
					JSONObject liveFeedObj = null;
					try {
						liveFeedObj = JSONparser
								.GetJSONAuthenticatedRequest(
										"http://www.rfm.fr/ws/geolocalisation/flux?lat=",
										Double.toString(mCurrentLocation.getLatitude()),Double.toString(mCurrentLocation.getLongitude()));
					} catch (Exception e) {
						e.printStackTrace();
					} finally {
						Radio radio = JSONparser
								.parseLiveFeed(liveFeedObj);
						writeJsonInFile(RFM_FILENAME,
								liveFeedObj.toString());
			
						if (ApplicationData.getMainModel().getRadios().size() == 2){
							ApplicationData.getMainModel().getRadios().add(0, radio);
						} else {
							ApplicationData.getMainModel().getRadios().set(0, radio);
						}
						LiveFragment.RADIO_GEOLOCALIZED = true;
						finish();
					}
					
				}
			}).start();			
		}		
	}

	@Override
	public void onConnected(Bundle arg0) {
		Log.v ("Location", "Connected");
		mCurrentLocation = mLocationClient
				.getLastLocation();
		
		if (mCurrentLocation == null){
			mLocationClient.requestLocationUpdates(mLocationRequest, this);
		}
	}

	@Override
	public void onDisconnected() {
		Log.v("Location", "Disconnected");
	}

	@Override
	public void onConnectionFailed(ConnectionResult arg0) {
		Log.v("Location", "Connection failed");
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		finish();
	}
}
